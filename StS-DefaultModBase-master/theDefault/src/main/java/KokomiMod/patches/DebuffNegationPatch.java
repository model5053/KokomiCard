package KokomiMod.patches;

import com.badlogic.gdx.Gdx;
import com.evacipated.cardcrawl.mod.stslib.actions.common.GainCustomBlockAction;
import com.evacipated.cardcrawl.modthespire.lib.*;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.actions.utility.TextAboveCreatureAction;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.powers.AbstractPower;
import com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect;
import javassist.CtBehavior;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SpirePatch(clz = ApplyPowerAction.class, method = "update")
public class DebuffNegationPatch {

    private static final Logger logger = LogManager.getLogger(DebuffNegationPatch.class.getName());


    @SpireInsertPatch(locator = Locator.class, localvars = {"duration","powerToApply"})
    public static SpireReturn DebuffNegation(@ByRef ApplyPowerAction[] __instance, @ByRef float[] ___duration, @ByRef AbstractPower[] ___powerToApply)
    {
        logger.info("Patches Debuff Negation!");
       // AbstractDungeon.actionManager.addToTop(new GainBlockAction());


        //Patches

        if (__instance[0].target.hasPower("KokomiMod:Purify") && ___powerToApply[0].type == AbstractPower.PowerType.DEBUFF) {
            logger.info("Condition True!");
            AbstractDungeon.actionManager.addToTop(new TextAboveCreatureAction(__instance[0].target, __instance[0].TEXT[0]));
            ___duration[0] -= Gdx.graphics.getDeltaTime();
            CardCrawlGame.sound.play("NULLIFY_SFX");
            __instance[0].target.getPower("KokomiMod:Purify").flashWithoutSound();
            return SpireReturn.Return();
        }
            return SpireReturn.Continue();
    }


    private static class Locator extends SpireInsertLocator{


        @Override
        public int[] Locate(CtBehavior ctMethodToPatch) throws Exception {

            Matcher finalMatcher = new Matcher.NewExprMatcher(FlashAtkImgEffect.class);

            return LineFinder.findInOrder(ctMethodToPatch, finalMatcher);
        }


    }


}
