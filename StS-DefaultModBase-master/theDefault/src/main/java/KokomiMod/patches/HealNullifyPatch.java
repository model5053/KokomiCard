package KokomiMod.patches;


import com.badlogic.gdx.Gdx;
import com.evacipated.cardcrawl.modthespire.lib.*;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.GainBlockAction;
import com.megacrit.cardcrawl.actions.common.HealAction;
import com.megacrit.cardcrawl.actions.utility.TextAboveCreatureAction;
import com.megacrit.cardcrawl.core.CardCrawlGame;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.powers.AbstractPower;
import com.megacrit.cardcrawl.vfx.combat.FlashAtkImgEffect;
import javassist.CtBehavior;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

@SpirePatch(clz = HealAction.class, method = "update")
public class HealNullifyPatch {

    private static final Logger logger = LogManager.getLogger(HealNullifyPatch.class.getName());


    @SpirePrefixPatch
    public static SpireReturn HealNullify(@ByRef HealAction[] __instance, @ByRef float[] ___duration) {
        logger.info("Patches Heal Nullify!");

        //Patches

        if (__instance[0].target.hasPower("KokomiMod:KokomiRejection")) {
            logger.info("Condition True!");
            AbstractDungeon.actionManager.addToTop(new TextAboveCreatureAction(__instance[0].target, "Nullified"));
            ___duration[0] -= Gdx.graphics.getDeltaTime();
            CardCrawlGame.sound.play("NULLIFY_SFX");
            __instance[0].target.getPower("KokomiMod:KokomiRejection").flashWithoutSound();
            __instance[0].isDone = true;
            return SpireReturn.Return();
        }
        return SpireReturn.Continue();
    }

}
