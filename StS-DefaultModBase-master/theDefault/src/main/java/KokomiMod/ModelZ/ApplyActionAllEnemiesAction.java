package KokomiMod.ModelZ;

import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.AbstractPower;

public class ApplyActionAllEnemiesAction extends AbstractGameAction {

    private int TIMES;
    private AbstractGameAction action;

    boolean isAddToBot = true;
    public ApplyActionAllEnemiesAction(AbstractGameAction action, int TIMES, boolean isAddToBot){
        this.action = action;
        actionType = ActionType.POWER;
        this.TIMES = TIMES;
        this.isAddToBot = isAddToBot;
    }

    @Override
    public void update() {

        for (int i = 0; i < TIMES; i++) {
            for(int j = 0; j < AbstractDungeon.getCurrRoom().monsters.monsters.size(); j++) {
                AbstractMonster mo = AbstractDungeon.getCurrRoom().monsters.monsters.get(j);
                action.target = mo;
                if(isAddToBot)
                    addToBot(action);
                else
                    addToTop(action);
            }

        }

        isDone = true;
    }
}
