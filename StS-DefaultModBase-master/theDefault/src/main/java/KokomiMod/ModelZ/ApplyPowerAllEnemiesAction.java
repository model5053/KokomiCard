package KokomiMod.ModelZ;

import KokomiMod.powers.KokomiRejection;
import com.megacrit.cardcrawl.actions.AbstractGameAction;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import com.megacrit.cardcrawl.powers.AbstractPower;
import com.megacrit.cardcrawl.powers.VulnerablePower;

public class ApplyPowerAllEnemiesAction extends AbstractGameAction {

    private int magicNumber;
    private int TIMES;
    private AbstractCreature source;
    private AbstractPower power;

    private boolean isAddToBot = true;

    public ApplyPowerAllEnemiesAction(AbstractCreature source, AbstractPower power, int magicNumber, int TIMES, boolean isAddToBot){
        this.power = power;
        this.source = source;
        this.magicNumber = magicNumber;
        actionType = ActionType.POWER;
        this.TIMES = TIMES;
        this.isAddToBot = isAddToBot;
    }

    @Override
    public void update() {

        for (int i = 0; i < TIMES; i++) {
            for(int j = 0; j < AbstractDungeon.getCurrRoom().monsters.monsters.size(); j++) {
                AbstractMonster mo = AbstractDungeon.getCurrRoom().monsters.monsters.get(j);
                power.owner = mo;
                if(isAddToBot)
                    addToBot(new ApplyPowerAction(mo, source, power, magicNumber));
                else
                    addToTop(new ApplyPowerAction(mo, source, power, magicNumber));
            }
        }

        isDone = true;
    }
}
