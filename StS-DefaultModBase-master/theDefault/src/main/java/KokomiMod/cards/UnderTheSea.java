package KokomiMod.cards;

import KokomiMod.powers.Splash;
import basemod.helpers.BaseModCardTags;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import KokomiMod.KokomiMod;
import KokomiMod.characters.Kokomi;
import KokomiMod.powers.RarePower;

import static KokomiMod.KokomiMod.makeCardPath;

public class UnderTheSea extends AbstractDynamicCard {

    /*
     * Wiki-page: https://github.com/daviscook477/BaseMod/wiki/Custom-Cards
     *
     * In-Progress Form At the start of your turn, play a TOUCH.
     */

    // TEXT DECLARATION 

    public static final String ID = KokomiMod.makeID(UnderTheSea.class.getSimpleName());
    public static final String IMG = makeCardPath("UnderTheSea.png");

    // /TEXT DECLARATION/


    // STAT DECLARATION 	

    private static final CardRarity RARITY = CardRarity.BASIC;
    private static final CardTarget TARGET = CardTarget.SELF;
    private static final CardType TYPE = CardType.SKILL;
    public static final CardColor COLOR = Kokomi.Enums.KOKOMI_COLOR;

    private static final int COST = 1;
    private static final int UPGRADE_COST = 1;

    private static final int MAGIC = 1;

    private static final int UPGRADE_MAGIC = 1;

    // /STAT DECLARATION/


    public UnderTheSea() {

        super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
        magicNumber = baseMagicNumber = MAGIC;

        //this.tags.add(BaseModCardTags.FORM); //Tag your strike, defend and form cards so that they work correctly.

    }

    // Actions the card should do.
    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {
        AbstractDungeon.actionManager.addToBottom(
                new ApplyPowerAction(p, p, new Splash(p, p, this.magicNumber), magicNumber)
        );
    }

    //Upgraded stats.
    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeMagicNumber(UPGRADE_MAGIC);
            initializeDescription();
        }
    }
}
