package KokomiMod.cards;

import KokomiMod.ModelZ.ApplyActionAllEnemiesAction;
import KokomiMod.ModelZ.ApplyPowerAllEnemiesAction;
import KokomiMod.ModelZ.CustomTags;
import KokomiMod.powers.KokomiRejection;
import basemod.helpers.BaseModCardTags;
import com.megacrit.cardcrawl.actions.common.ApplyPowerAction;
import com.megacrit.cardcrawl.actions.common.HealAction;
import com.megacrit.cardcrawl.actions.common.RemoveAllBlockAction;
import com.megacrit.cardcrawl.characters.AbstractPlayer;
import com.megacrit.cardcrawl.core.AbstractCreature;
import com.megacrit.cardcrawl.dungeons.AbstractDungeon;
import com.megacrit.cardcrawl.monsters.AbstractMonster;
import KokomiMod.KokomiMod;
import KokomiMod.characters.Kokomi;
import com.megacrit.cardcrawl.powers.AbstractPower;

import static KokomiMod.KokomiMod.makeCardPath;

public class KokomiBurst extends AbstractDynamicCard {

    /*
     * Wiki-page: https://github.com/daviscook477/BaseMod/wiki/Custom-Cards
     *
     * In-Progress Form At the start of your turn, play a TOUCH.
     */

    // TEXT DECLARATION 

    public static final String ID = KokomiMod.makeID(KokomiBurst.class.getSimpleName());
    public static final String IMG = makeCardPath("KokomiBurst.png");

    // /TEXT DECLARATION/


    // STAT DECLARATION 	

    private static final CardRarity RARITY = CardRarity.RARE;
    private static final CardTarget TARGET = CardTarget.SELF;
    private static final CardType TYPE = CardType.SKILL;
    public static final CardColor COLOR = Kokomi.Enums.KOKOMI_COLOR;

    private static final int COST = 2;

    private static final int MAGIC = 12;

    private static final int UPGRADE_MAGIC = 6;
    // /STAT DECLARATION/


    public KokomiBurst() {

        super(ID, IMG, COST, TYPE, COLOR, RARITY, TARGET);
        magicNumber = baseMagicNumber = MAGIC;

        this.tags.add(CustomTags.KOKOMI_HEALING); //Tag your strike, defend and form cards so that they work correctly.

    }

    // Actions the card should do.
    @Override
    public void use(AbstractPlayer p, AbstractMonster m) {

        addToBot(new HealAction(p, p, magicNumber));

       if(p.hasPower("KokomiMod:Splash")){
           addToTop(new ApplyActionAllEnemiesAction(new RemoveAllBlockAction(p, p),1,false));
           addToBot(new ApplyPowerAllEnemiesAction(p, new KokomiRejection(p, p, 1),1,1,true));
       }

        /*  for(int j = 0; j < AbstractDungeon.getCurrRoom().monsters.monsters.size(); j++) {
               AbstractMonster mo = AbstractDungeon.getCurrRoom().monsters.monsters.get(j);
               AbstractDungeon.actionManager.addToBottom(
                       new ApplyPowerAction(mo, p, new KokomiRejection(mo, p, 1), 1));
           }*/


    }

    //Upgraded stats.
    @Override
    public void upgrade() {
        if (!upgraded) {
            upgradeName();
            upgradeMagicNumber(UPGRADE_MAGIC);
            initializeDescription();
        }
    }
}
